Pod::Spec.new do |s|

s.platform = :ios
s.ios.deployment_target = '12.0'
s.name = "CFPartnerSDKiOS"
s.summary = "The CoinFlip iOS Partner SDK allows your view our ATM locations to purchase Bitcoin, Ethereum and more with cash instantly."
s.requires_arc = true
s.version = "0.1.0"
s.license = 'LICENSE.md'
s.author = { "Husein Kareem" => "hkareem@coinflip.tech" }
s.homepage = "https://gitlab.com/husein_coinflip/cfpartnersdkios"
s.source = { :git => "https://gitlab.com/husein_coinflip/cfpartnersdkios",
             :tag => "#{s.version}" }
s.framework = "UIKit", "MapKit", "CoreLocation"
s.source_files = "CFPartnerSDKiOS/CFPartnerSDKiOS/**/*.swift"
#s.resources = "CFPartnerSDKiOS/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"
s.swift_version = "5.0"

end
