//
//  ATMFinderViewController.swift
//  CFPartnerSDKiOS
//
//  Created by Husein Kareem on 1/14/22.
//

import Foundation
import UIKit
import GoogleMaps

class ATMFinderViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MapsService().mapsServiceWithProvider(providerName: "Google")
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
        let mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        self.view.addSubview(mapView)
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
        marker.title = "Sydney"
        marker.snippet = "Australia"
        marker.map = mapView
    }
    
}
